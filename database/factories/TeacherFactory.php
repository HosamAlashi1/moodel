<?php

namespace Database\Factories;

use App\Models\Teacher;
use Illuminate\Database\Eloquent\Factories\Factory;

class TeacherFactory extends Factory
{
    protected $model = Teacher::class;

    public function definition()
    {
        return [
            'name_ar' => $this->faker->name,
            'age_ar' => $this->faker->numberBetween(29, 55),
            'name_en' => $this->faker->name,
            'age_en' => $this->faker->numberBetween(29, 55),
        ];
    }
}
