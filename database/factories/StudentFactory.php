<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    protected $model = Student::class;

    public function definition()
    {
        return [
            'name_ar' => $this->faker->name,
            'age_ar' => $this->faker->numberBetween(18, 25),
            'name_en' => $this->faker->name,
            'age_en' => $this->faker->numberBetween(18, 25),
        ];
    }
}
