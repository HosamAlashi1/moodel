<?php

namespace Database\Factories;

use App\Models\Course;
use Illuminate\Database\Eloquent\Factories\Factory;

class CourseFactory extends Factory
{
    protected $model = Course::class;

    public function definition()
    {
        return [
            'name_ar' => $this->faker->name,
            'name_en' => $this->faker->name,
            'number' => $this->faker->numberBetween(1148, 4435),
        ];
    }
}
