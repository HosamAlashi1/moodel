<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Course;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

                Teacher::factory(20)->create();

        Teacher::factory()->create([
            'name_ar' => Str::random(7),
            'age_ar' => mt_rand(18, 25), // Generate a random age between 18 and 25
            'name_en' => Str::random(7),
            'age_en' => mt_rand(18, 25), // Generate a random age between 18 and 25
        ]);


        Student::factory(20)->create();

        Student::factory()->create([
            'name_ar' => Str::random(7),
            'age_ar' => mt_rand(18, 25), // Generate a random age between 18 and 25
            'name_en' => Str::random(7),
            'age_en' => mt_rand(18, 25), // Generate a random age between 18 and 25
        ]);

        Course::factory(5000)->create();

        Course::factory()->create([
            'name_ar' => Str::random(7),
            'name_en' => mt_rand(18, 25), // Generate a random age between 18 and 25
            'number' => Str::random(7),
        ]);
    }
}
