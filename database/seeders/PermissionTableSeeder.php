<?php
namespace Database\Seeders;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permissions
        $permissions = [
            'display admin',
            'edit admin',
            'delete admin',
            'create admin',
            'export-pdf admin',
            'import-excel admin',
            'export-excel admin',

            'display roles',
            'create roles',
            'edit roles',
            'delete roles',

            'display course',
            'create course',
            'edit course',
            'delete course',
            'export-pdf course',
            'import-excel course',
            'export-excel course',

            'display teacher',
            'edit teacher',
            'delete teacher',
            'create teacher',
            'display teacher courses',
            'create teacher courses',
            'delete teacher courses',
            'export-pdf teacher',
            'import-excel teacher',
            'export-excel teacher',


            'display student',
            'edit student',
            'delete student',
            'create student',
            'display student courses',
            'create student courses',
            'delete student courses',
            'export-pdf student',
            'import-excel student',
            'export-excel student',
        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }
}
