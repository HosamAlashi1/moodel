<?php

use App\DataTables\AdminsDataTableEditor;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseStudentController;
use App\Http\Controllers\CourseTeacherController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
define('PAGINATION_COUNT', 5);


//Route::group(['middleware'=>['guest']],function (){ // cannot reach this route if has login
    Route::get('/admin/login', [LoginController::class, 'showLoginForm']);
    Route::post('/admin/login', [LoginController::class, 'login'])->name('login');
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
//});
//Auth::routes();

Route::get('/',function (){
    return redirect('/admin');
});

Route::group(
    ['prefix' => LaravelLocalization::setLocale(). '/admin',
        'middleware' => [ 'auth','localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],function (){
    Route::get('/',[DashboardController::class, 'index'])->name('main');


    Route::get('/index',[AdminController::class, 'index'])->name('admin.index');
//    Route::get('/all',[AdminController::class, 'getUserDatatable'])->name('admin.getUserDatatable');
    Route::get('/create',[AdminController::class, 'create'])->name('admin.create');
    Route::post('/store',[AdminController::class, 'store'])->name('admin.store');
    Route::get('/edit/{id}', [AdminController::class, 'edit'])->name('admin.edit');
    Route::post('/update/{id}', [AdminController::class, 'update'])->name('admin.update');
    Route::get('/delete/{id}', [AdminController::class, 'destroy'])->name('admin.delete');
    Route::get('/export',[AdminController::class, 'export'])->name('admin.export');
    Route::post('/import',[AdminController::class, 'import'])->name('admin.import');
    Route::get('/export-pdf',[AdminController::class, 'exportPdf'])->name('admin.export-pdf');
    Route::get('/payment',[AdminController::class, 'payment'])->name('admin.payment');

    ############################ Begin role Routes  ############################
    Route::group(['prefix' => 'roles'],function (){
        Route::get('/',[RoleController::class, 'index'])->name('roles.index');
        Route::get('/create',[RoleController::class, 'create'])->name('roles.create');
        Route::post('/store', [RoleController::class, 'store'])->name('roles.store');
        Route::get('/show/{id}', [RoleController::class, 'show'])->name('roles.show');
        Route::get('/edit/{id}', [RoleController::class, 'edit'])->name('roles.edit');
        Route::post('/update/{id}', [RoleController::class, 'update'])->name('roles.update');
        Route::get('/delete/{id}', [RoleController::class, 'destroy'])->name('roles.delete');
    });
    ############################ End role Routes  ############################

        ############################ Begin student Routes  ############################
        Route::group(['prefix' => 'student'],function (){
            Route::get('/',[StudentController::class, 'index'])->name('student.index');
//            Route::get('/all',[StudentController::class, 'getUserDatatable'])->name('student.getUserDatatable');
            Route::get('/create',[StudentController::class, 'create'])->name('student.create');
            Route::post('/store', [StudentController::class, 'store'])->name('student.store');
            Route::get('/edit/{id}', [StudentController::class, 'edit'])->name('student.edit');
            Route::post('/update/{id}', [StudentController::class, 'update'])->name('student.update');
            Route::get('/delete/{id}', [StudentController::class, 'destroy'])->name('student.delete');
            Route::get('/export-pdf',[StudentController::class, 'exportPdf'])->name('student.export-pdf');
            Route::get('/export',[StudentController::class, 'export'])->name('student.export');
            Route::post('/import',[StudentController::class, 'import'])->name('student.import');
            Route::get('/export-pdf',[StudentController::class, 'exportPdf'])->name('student.export-pdf');
        });
        ############################ End student Routes  ############################

        ############################ Begin teacher Routes  ############################
        Route::group(['prefix' => 'teacher'],function (){
            Route::get('/',[TeacherController::class, 'index'])->name('teacher.index');
            Route::get('/create',[TeacherController::class, 'create'])->name('teacher.create');
            Route::post('/store', [TeacherController::class, 'store'])->name('teacher.store');
            Route::get('/edit/{id}', [TeacherController::class, 'edit'])->name('teacher.edit');
            Route::post('/update/{id}', [TeacherController::class, 'update'])->name('teacher.update');
            Route::get('/delete/{id}', [TeacherController::class, 'destroy'])->name('teacher.delete');
            Route::get('/export',[TeacherController::class, 'export'])->name('teacher.export');
            Route::post('/import',[TeacherController::class, 'import'])->name('teacher.import');
            Route::get('/export-pdf',[TeacherController::class, 'exportPdf'])->name('teacher.export-pdf');
        });
        ############################ End teacher Routes  ############################

        ############################ Begin course Routes  ############################
        Route::group(['prefix' => 'course'],function (){
            Route::get('/',[CourseController::class, 'index'])->name('course.index');
            Route::get('/all',[CourseController::class, 'getUserDatatable'])->name('course.getUserDatatable');
            Route::get('/create',[CourseController::class, 'create'])->name('course.create');
            Route::post('/store', [CourseController::class, 'store'])->name('course.store');
            Route::get('/edit/{id}', [CourseController::class, 'edit'])->name('course.edit');
            Route::post('/update/{id}', [CourseController::class, 'update'])->name('course.update');
            Route::get('/delete/{id}', [CourseController::class, 'destroy'])->name('course.delete');
            Route::get('/export',[CourseController::class, 'export'])->name('course.export');
            Route::post('/import',[CourseController::class, 'import'])->name('course.import');
            Route::get('/export-pdf',[CourseController::class, 'exportPdf'])->name('course.export-pdf');
        });
        ############################ End course Routes  ############################

        Route::group(['prefix' => 'course/student'],function (){
            Route::get('/{id}',[CourseStudentController::class, 'index'])->name('student.course.index');
            Route::get('/create/{student_id}',[CourseStudentController::class, 'create'])->name('student.course.create');
            Route::post('/store/{student_id}', [CourseStudentController::class, 'store'])->name('student.course.store');
            Route::get('/delete/{course_id}/{student_id}', [CourseStudentController::class, 'destroy'])->name('student.course.delete');
        });


        Route::group(['prefix' => 'course/teacher'],function (){
            Route::get('/{id}',[CourseTeacherController::class, 'index'])->name('teacher.course.index');
            Route::get('/create/{teacher_id}',[CourseTeacherController::class, 'create'])->name('teacher.course.create');
            Route::post('/store/{teacher_id}', [CourseTeacherController::class, 'store'])->name('teacher.course.store');
            Route::get('/delete/{course_id}/{teacher_id}', [CourseTeacherController::class, 'destroy'])->name('teacher.course.delete');
        });
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
