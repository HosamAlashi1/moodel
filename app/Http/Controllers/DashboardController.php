<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Teacher;
use Illuminate\Http\Request;
use Fx3costa\LaravelChartJs\Builder as ChartJs;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    public function index()
    {
        $lastWeek = $this->getLastWeekLabels();
        $datasets = $this->generateDatasets($lastWeek);

        $chartOptions = [
            'legend' => [
                'display' => true,
                'labels' => [
                    'fontColor' => '#333',
                    'fontSize' => 12,
                ],
            ],
            'scales' => [
                'xAxes' => [[
                    'ticks' => [
                        'fontColor' => '#333',
                    ],
                ]],
                'yAxes' => [[
                    'ticks' => [
                        'beginAtZero' => true,
                        'fontColor' => '#333',
                    ],
                ]],
            ],
        ];

        $barChart = $this->createChart('barChart', 'bar', $lastWeek, $datasets, $chartOptions);
        $lineChart = $this->createChart('lineChart', 'line', $lastWeek, $datasets, $chartOptions);

        return view('dashboard.dashboard', compact('barChart', 'lineChart'));
    }

    private function createChart($name, $type, $labels, $datasets, $options)
    {
        return app()->chartjs
            ->name($name)
            ->type($type)
            ->size(['width' => 800, 'height' => 400])
            ->labels($labels)
            ->datasets($datasets)
            ->options($options);
    }

    private function getLastWeekLabels()
    {
        $lastWeek = collect([]);
        $labels = [];

        for ($i = 6; $i >= 0; $i--) {
            $day = now()->subDays($i);
            $lastWeek->push($day->format('l'));
            $labels[] = $day->format('F j');
        }

        return $labels;
    }

    private function generateDatasets($labels)
    {
        $datasets = [];

        for ($i = 6; $i >= 0; $i--) {
            $day = now()->subDays($i);
            $startDate = $day->copy()->startOfDay();
            $endDate = $day->copy()->endOfDay();

            $teacherCount = DB::table('teachers')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->count();

            $teacherDataset[] = $teacherCount;

            $studentCount = DB::table('students')
                ->whereBetween('created_at', [$startDate, $endDate])
                ->count();

            $studentDataset[] = $studentCount;
        }

        $datasets[] = [
            "label" => __('messages.Teacher'),
            'backgroundColor' => "#0162e8",
            'borderColor' => "#0162e8",
            "pointBorderColor" => "#0162e8",
            "pointBackgroundColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => "#0162e8",
            'data' => $teacherDataset,
        ];

        $datasets[] = [
            "label" => __('messages.Student'),
            'backgroundColor' => "#f95374",
            'borderColor' => "#f95374",
            "pointBorderColor" => "#f95374",
            "pointBackgroundColor" => "#fff",
            "pointHoverBackgroundColor" => "#fff",
            "pointHoverBorderColor" => "#f95374",
            'data' => $studentDataset,
        ];

        return $datasets;
    }


}
