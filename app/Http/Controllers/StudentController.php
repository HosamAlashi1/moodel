<?php

namespace App\Http\Controllers;

use App\DataTables\StudentDataTable;
use App\Exports\ExportStudent;
use App\Http\Requests\ExportRequest;
use App\Http\Requests\Teach_Std_Request;
use App\Imports\ImportStudent;
use App\Models\Student;
use App\Traits\AdminTrait;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class StudentController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:display student', ['only' => ['index']]);
        $this->middleware('permission:create student', ['only' => ['create','store']]);
        $this->middleware('permission:edit student', ['only' => ['edit','update']]);
        $this->middleware('permission:delete student', ['only' => ['destroy']]);
    }
    use AdminTrait;
    /**
     * Display a listing of the resource.
     */
//    public function index(Request $request)
//    {
//        $search = $request->input('search');
//
//        $query = Student::with('course_students.courses.course_teachers.teachers')
//            ->orderBy('created_at', 'desc');
//
//        if ($search) {
//            $query->where(function ($q) use ($search) {
//                $q->where('name_ar', 'like', '%'.$search.'%')
//                    ->orWhere('name_en', 'like', '%'.$search.'%');
//            });
//        }
//
//        $students = $query->paginate(PAGINATION_COUNT);
//
//        return view('students.index', compact('students', 'search'));
//    }

    public function index(StudentDataTable $dataTable)
    {
        return $dataTable->render('students.index');
    }

//
//    public function getUserDatatable()
//    {
//        $students = Student::with('course_students.courses.course_teachers.teachers')->get();
//
//        return DataTables::of($students)
//            ->addIndexColumn()
//            ->addColumn('photo', function ($student) {
//                // Return the photo URL or null if it doesn't exist
//                return $student->photo ? asset($student->photo) : null;
//            })
//            ->addColumn('action', function ($student) {
//                $editButton = '';
//                $deleteButton = '';
//                $coursesButton = '';
//
//                if (auth()->user()->can('edit student')) {
//                    $editButton = '<a href="' . route('admin.edit', $student->id) . '" type="button" class="btn btn-primary">' . __('messages.Edit') . '</a>';
//                }
//
//                if (auth()->user()->can('delete student')) {
//                    $deleteButton = '<a href="' . route('admin.delete', $student->id) . '" type="button" class="btn btn-danger">' . __('messages.Delete') . '</a>';
//                }
//
//                if (auth()->user()->can('display student courses')) {
//                    $coursesButton = '<a href="' . route('student.course.index', $student->id) . '" type="button" class="btn btn-success">' . __('messages.Courses') . '</a>';
//                }
//
//                return $editButton . ' ' . $deleteButton . ' ' . $coursesButton;
//            })
//            ->rawColumns(['photo', 'action'])
//            ->make(true);
//    }
//


    public function create(){
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Teach_Std_Request $request)
    {
        $image_path = '';
        try {
            if ($request->has('photo')) {
                $image_path = $this->uploadImage('admin', $request->photo);
            }

            $Student = Student::create([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'age_ar' => $request->age_ar,
                'age_en' => $request->age_en,
                'photo' => $image_path,
            ]);
            return redirect()->route('student.index')->with(['success' => __('messages.Added successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('student.create')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $student = Student::find($id);
        if (!$student)
            return redirect()->route('student.index')->with(['error' =>  __('messages.The student is not exist')]);
        return view('students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Teach_Std_Request $request, $id)
    {
        try {
            $student = Student::orderBy('created_at', 'desc')->get();
            if (!$student) {
                return redirect()->route('student.index')->with(['error' =>  __('messages.The student is not exist')]);
            }

            if ($request->has('photo')) {
                $image_path = $this->uploadImage('admin', $request->photo);
                Student::where('id', $id)->update([
                    'photo' => $image_path
                ]);
            }

            Student::where('id', $id)->update([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'age_ar' => $request->age_ar,
                'age_en' => $request->age_en,
            ]);

            return redirect()->route('student.index')->with(['success' => __('messages.updated successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('student.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $Student = Student::find($id);
            if (!$Student) {
                return redirect()->route('student.index')->with(['error' =>  __('messages.The student is not exist')]);
            }
            $Student->delete();
            return redirect()->route('student.index')->with(['success' => __('messages.deleted successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('student.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    public function export(){
        return Excel::download(new ExportStudent, 'students.xlsx');
    }

    public function import(ExportRequest $request){
        Excel::import(new ImportStudent, $request->file('file')->store('files'));
        return redirect()->back();
    }

    public function exportPdf(Request $request)
    {
        $students = Student::with('course_students.courses.course_teachers.teachers')->orderBy('created_at', 'desc')->get();

        $data = [
            'title' => 'Welcome !!',
            'date' => date('d/m/Y'),
            'students' => $students
        ];
        if ($request->has('download')) {
            $pdf = PDF::loadView('students.pdf.index', $data);
            return $pdf->download('students.pdf');
        }

        return view('students.index', $data);
    }

}
