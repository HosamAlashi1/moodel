<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseTeacher;
use App\Models\Teacher;
use Illuminate\Http\Request;

class CourseTeacherController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:display teacher courses', ['only' => ['index']]);
        $this->middleware('permission:create teacher courses', ['only' => ['create','store']]);
        $this->middleware('permission:delete teacher courses', ['only' => ['destroy']]);
    }

    public function index($id){
        try {
            $teacher= Teacher::with('course_teachers.courses')->findOrFail($id);
            if (!$teacher)
                return redirect()->route('teacher.index')->with(['error' =>  __('messages.The teacher is not exist')]);
            $courses = $teacher->course_teachers()->with('courses')->paginate(PAGINATION_COUNT);
            return view('teachers.course.index',compact(['courses','id']));
        } catch (\Exception $ex) {
            return redirect()->route('teacher.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }
    public function create($teacher_id){
        try {
            $teacher = Teacher::findOrFail($teacher_id);
            if (!$teacher)
                return redirect()->route('teacher.index')->with(['error' => __('messages.The teacher is not exist')]);
            $courses = Course::all();
            return view('teachers.course.create',compact(['courses','teacher_id']));
        } catch (\Exception $ex) {
            return redirect()->route('teacher.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    public function store(Request $request,$teacher_id){
        try {
            CourseTeacher::create([
                'course_id' => $request->course_id,
                'teacher_id' =>$teacher_id,
            ]);
            return redirect()->route('teacher.index', $teacher_id)->with(['success' => __('messages.Added successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('teacher.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    public function destroy($course_id, $teacher_id)
    {
        try {
            CourseTeacher::where('course_id', $course_id)
                ->where('teacher_id', $teacher_id)
                ->delete();

            return redirect()->route('teacher.index', $teacher_id)->with(['success' => __('messages.deleted successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('teacher.index', $teacher_id)->with(['error' => __('messages.There was an error try again')]);
        }
    }
}
