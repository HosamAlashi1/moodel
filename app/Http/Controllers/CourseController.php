<?php

namespace App\Http\Controllers;

use App\DataTables\CourseDataTable;
use App\Exports\ExportCourse;
use App\Http\Requests\CourseRequest;
use App\Http\Requests\ExportRequest;
use App\Imports\ImportCourse;
use App\Models\Course;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class CourseController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:display course', ['only' => ['index']]);
        $this->middleware('permission:create course', ['only' => ['create','store']]);
        $this->middleware('permission:edit course', ['only' => ['edit','update']]);
        $this->middleware('permission:delete course', ['only' => ['destroy']]);
    }
    /**
     * Display a listing of the resource.
     */

    public function index(CourseDataTable $dataTable)
    {
        return $dataTable->render('courses.index');
    }


    public function create(){
        return view('courses.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CourseRequest $request)
    {
        try {
            $course = Course::create([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'number' => $request->number,
            ]);
            return redirect()->route('course.index')->with(['success' => __('messages.Created successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('course.create')->with(['error' => __('messages.Created successfully')]);
        }
    }


    /**
     * Show the form for editing the specified resource.
     */

    public function edit($id)
    {
        $course = Course::find($id);
        if (!$course)
            return redirect()->route('course.index')->with(['error' => __('messages.The student is not exist')]);
        return view('courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CourseRequest $request,$id)
    {
        try {
            $course = Course::find($id);
            if (!$course)
                return redirect()->route('course.index')->with(['error' => __('messages.The student is not exist')]);
            Course::where('id', $id)->update([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'number' => $request->number,
            ]);
            return redirect()->route('course.index')->with(['success' => __('messages.updated successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('course.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $course = Course::find($id);
            if (!$course) {
                return redirect()->route('course.index')->with(['error' =>  __('messages.The Course is not exist')]);
            }
            $course->delete();
            return redirect()->route('course.index')->with(['success' => __('messages.deleted successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('course.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    public function export(){
        return Excel::download(new ExportCourse, 'courses.xlsx');
    }

    public function import(ExportRequest $request){
        Excel::import(new ImportCourse, $request->file('file')->store('files'));
        return redirect()->back();
    }

    public function exportPdf(Request $request)
    {
        $courses = Course::with('course_students.courses.course_teachers.teachers')->orderBy('created_at', 'desc')->get();

        $data = [
            'title' => 'Welcome !!',
            'date' => date('d/m/Y'),
            'students' => $courses
        ];

        if ($request->has('download')) {
            $pdf = PDF::loadView('courses.pdf.index', $data);
            return $pdf->download('courses.pdf');
        }

        return view('courses.index', $data);
    }

}
