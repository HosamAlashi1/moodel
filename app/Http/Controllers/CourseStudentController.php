<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseStudent;
use App\Models\Student;
use Illuminate\Http\Request;

class CourseStudentController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:display student courses', ['only' => ['index']]);
        $this->middleware('permission:create student courses', ['only' => ['create','store']]);
        $this->middleware('permission:delete student courses', ['only' => ['destroy']]);
    }

    public function index($id){
        try {
            $student = Student::with('course_students.courses')->findOrFail($id);
            if (!$student)
                return redirect()->route('student.index')->with(['error' => __('messages.The student is not exist')]);
            $courses = $student->course_students()->with('courses')->paginate(PAGINATION_COUNT);
            return view('students.course.index',compact(['courses','id']));
        } catch (\Exception $ex) {
            return redirect()->route('student.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }
    public function create($student_id){
        try {
            $student = Student::findOrFail($student_id);
            if (!$student)
                return redirect()->route('student.index')->with(['error' => __('messages.The student is not exist')]);
            $courses = Course::all();
            return view('students.course.create',compact(['courses','student_id']));
        } catch (\Exception $ex) {
            return redirect()->route('student.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    public function store(Request $request,$student_id){
        try {
            CourseStudent::create([
                'course_id' => $request->course_id,
                'student_id' =>$student_id,
            ]);
            return redirect()->route('student.index', $student_id)->with(['success' => __('messages.Added successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('student.index')->with(['error' => 'There was an error try again']);
        }
    }

    public function destroy($course_id, $student_id)
    {
        try {
            CourseStudent::where('course_id', $course_id)
                ->where('student_id', $student_id)
                ->delete();
            return redirect()->route('student.index', $student_id)->with(['success' => __('messages.deleted successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('student.index', $student_id)->with(['error' => __('messages.There was an error try again')]);
        }
    }
}
