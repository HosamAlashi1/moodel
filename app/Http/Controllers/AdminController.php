<?php

namespace App\Http\Controllers;

use App\DataTables\AdminsDataTable;
use App\Exports\ExportAdmin;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\ExportRequest;
use App\Imports\ImportAdmin;
use App\Mail\mailRegister;
use App\Models\User;
use App\Traits\AdminTrait;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Yajra\DataTables\DataTables;


class AdminController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:display admin', ['only' => ['index']]);
        $this->middleware('permission:create admin', ['only' => ['create','store']]);
        $this->middleware('permission:edit admin', ['only' => ['edit','update']]);
        $this->middleware('permission:delete admin', ['only' => ['destroy']]);
    }

    use AdminTrait;

    public function index(AdminsDataTable $dataTable)
    {
        return $dataTable->render('admins.index');
    }

//    public function getUserDatatable()
//    {
//        $admins = User::orderBy('id', 'desc')->get();
//
//        return DataTables::of($admins)
//            ->addIndexColumn()
//            ->addColumn('photo', function ($admin) {
//                // Return the photo URL or null if it doesn't exist
//                return $admin->photo ? asset($admin->photo) : null;
//            })
//            ->addColumn('roles_name', function ($admin) {
//                // Return a comma-separated string of role names
//                return implode(', ', $admin->getRoleNames()->toArray());
//            })
//            ->addColumn('action', function ($admin) {
//                $editButton = '';
//                $deleteButton = '';
//
//                if (auth()->user()->can('edit admin')) {
//                    $editButton = '<a href="' . route('admin.edit', $admin->id) . '" type="button" class="btn btn-primary">' . __('messages.Edit') . '</a>';
//                }
//
//                if (auth()->user()->can('delete admin')) {
//                    $deleteButton = '<a href="javascript:void(0)" id="delete-user" data-url="' . route('admin.delete', $admin->id) . '" class="btn btn-danger">' . __('messages.Delete') . '</a>';
//                }
//
//                return $editButton . ' ' . $deleteButton;
//            })
//            ->rawColumns(['photo', 'action'])
//            ->make(true);
//    }



//    public function index(Request $request){
//
//        $search = $request->input('search');
//
//        $query = User::orderBy('id','desc');
//
//        if ($search) {
//            $query->where(function ($q) use ($search) {
//                $q->where('name_ar', 'like', '%'.$search.'%')
//                    ->orWhere('name_en', 'like', '%'.$search.'%');
//            });
//        }
//
//        $admins = $query->paginate(PAGINATION_COUNT);
//
//        return view('admins.index',compact('admins', 'search'));
//    }

    public function create(){
        $roles = Role::pluck('name','name')->all();
        return view('admins.create',compact('roles'));
    }

    public function store(AdminRequest $request){
        $image_path = '';
        DB::beginTransaction();
        try {
        if ($request->has('photo')) {
            $image_path = $this->uploadImage('admin', $request->photo);
        }

        $admin = User::create([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
            'roles_name' => $request->roles,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' =>Hash::make($request['password']),
            'photo' => $image_path,
        ]);
//            Mail::to($admin->email)->send(new mailRegister($admin));
            $admin->assignRole($request->input('roles'));
        DB::commit();
        toastr()->success(__('messages.Created successfully'));
        return redirect()->route('admin.index');
    } catch (\Exception $ex) {
        DB::rollBack();
        toastr()->error(__('messages.There was an error try again'));
        return redirect()->route('admin.create');
        }
    }

    public function edit($id)
    {
        try {
            $admin = User::find($id);
            if (!$admin) {
                toastr()->error( __('messages.The Admin is not exist'));
                return redirect()->route('admin.index');
            }
            $roles = Role::pluck('name','name')->all();
            $userRole = $admin->roles->pluck('name','name')->all();
            return view('admins.edit',compact('admin','roles','userRole'));
        }catch (\Exception $ex) {
            toastr()->error(__('messages.There was an error try again'));
            return redirect()->route('admin.index');
        }
    }

    public function update($id, AdminRequest $request)
    {
        DB::beginTransaction();

        try {
            $image_path = '';

            if ($request->has('photo')) {
                $image_path = $this->uploadImage('admin', $request->photo);
            }

            $admin = User::find($id); // Retrieve the User model instance

            $admin->update([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'user_name' => $request->user_name,
                'roles_name' => $request->roles,
                'email' => $request->email,
                'photo' => $image_path,
            ]);

            if (!empty($request['password'])) {
                $password = Hash::make($request['password']);
                $admin->update([
                    'password' => $password
                ]);
            }

            DB::table('model_has_roles')->where('model_id', $id)->delete();
            $admin->assignRole($request->input('roles'));
            DB::commit();
            toastr()->success( __('messages.updated successfully'));
            return redirect()->route('admin.index');
        } catch (\Exception $ex) {
            DB::rollBack();
            toastr()->error( __('messages.There was an error, please try again'));
            return redirect()->route('admin.edit', $id);
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $admin= User::find($id);
            if (!$admin) {
                toastr()->error( __('messages.The Admin is not exist'));
                return redirect()->route('admin.index');
            }
            $admin->delete();
            toastr()->error( __('messages.deleted successfully'),'Delete');
            return redirect()->route('admin.index');
        } catch (\Exception $ex) {
            toastr()->error(__('messages.There was an error try again'));
            return redirect()->route('admin.index');
        }
    }

    public function export(){
        return Excel::download(new ExportAdmin, 'admins.xlsx');
    }

    public function import(ExportRequest $request){
        Excel::import(new ImportAdmin, $request->file('file')->store('files'));
        return redirect()->back();
    }

    public function exportPdf(Request $request)
    {
        $admins = User::orderBy('id','desc')->get();

        $data = [
            'title' => 'Welcome !!',
            'date' => date('d/m/Y'),
            'admins' => $admins
        ];
        if ($request->has('download')) {
            $pdf = PDF::loadView('admins.pdf.index', $data);
            return $pdf->download('admins.pdf');
        }

        return view('admins.index', $data);
    }

    public function payment()
    {
        $data['amount'] = 1;
        $data['currency'] = 'KWD';
        $data['customer']['first_name'] = 'hosam';
        $data['customer']['email'] = 'hosam@gmail.com';
        $data['customer']['phone']['country_code'] = '972';
        $data['customer']['phone']['number'] = '594136058';
        $data['source']['id'] = 'src_card';
        $data['redirect']['url'] = 'http://127.0.0.1:8000/ar/admin/index';

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer sk_test_XKokBfNWv6FIYuTMg5sLPjhJ',
        ];

        $client = new Client();

        try {
            $response = $client->post('https://api.tap.company/v2/charges', [
                'headers' => $headers,
                'json' => $data,
            ]);

            $responseData = json_decode($response->getBody(), true);

            return redirect()->to($responseData['transaction']['url']);
        } catch (RequestException $e) {
            // Handle any exceptions or errors
            // You can log the error or show an error message
            return response('Error occurred while processing the payment.', 500);
        }
    }

}
