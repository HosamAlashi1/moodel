<?php

namespace App\Http\Controllers;

use App\DataTables\TeacherDataTable;
use App\Exports\ExportTeacher;
use App\Http\Requests\ExportRequest;
use App\Http\Requests\Teach_Std_Request;
use App\Imports\ImportTeacher;
use App\Models\Course;
use App\Models\CourseTeacher;
use App\Models\Teacher;
use App\Traits\AdminTrait;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class TeacherController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:display teacher', ['only' => ['index']]);
        $this->middleware('permission:create teacher', ['only' => ['create','store']]);
        $this->middleware('permission:edit teacher', ['only' => ['edit','update']]);
        $this->middleware('permission:delete teacher', ['only' => ['destroy']]);
    }
    use AdminTrait;
    /**
     * Display a listing of the resource.
     */
    public function index(TeacherDataTable $dataTable)
    {
        return $dataTable->render('teachers.index');
    }


    public function create(){
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Teach_Std_Request $request)
    {
        $image_path = '';
        try {
            if ($request->has('photo')) {
                $image_path = $this->uploadImage('admin', $request->photo);
            }

            $teacher = Teacher::create([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'age_ar' => $request->age_ar,
                'age_en' => $request->age_en,
                'photo' => $image_path,
            ]);
            return redirect()->route('teacher.index')->with(['success' => __('messages.Added successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('teacher.create')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $teacher = Teacher::find($id);
        if (!$teacher)
            return redirect()->route('teacher.index')->with(['error' => __('messages.The teacher is not exist')]);
        return view('teachers.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Teach_Std_Request $request,$id)
    {
        try {
            $teacher = Teacher::find($id);
            if (!$teacher)
                return redirect()->route('student.index')->with(['error' => __('messages.The teacher is not exist')]);
            if ($request->has('photo')) {
                $image_path = $this->uploadImage('admin', $request->photo);
                Teacher::where('id', $id)->update([
                    'photo' => $image_path
                ]);
            }
            Teacher::where('id', $id)->update([
                'name_ar' => $request->name_ar,
                'name_en' => $request->name_en,
                'age_ar' => $request->age_ar,
                'age_en' => $request->age_en,
            ]);
            return redirect()->route('teacher.index')->with(['success' => __('messages.updated successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('teacher.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            $teacher = Teacher::find($id);
            if (!$teacher) {
                return redirect()->route('teacher.index')->with(['error' => __('messages.The teacher is not exist')]);
            }
            $teacher->delete();
            return redirect()->route('teacher.index')->with(['success' => __('messages.deleted successfully')]);
        } catch (\Exception $ex) {
            return redirect()->route('teacher.index')->with(['error' => __('messages.There was an error try again')]);
        }
    }

    public function export(){
        return Excel::download(new ExportTeacher, 'teachers.xlsx');
    }

    public function import(ExportRequest $request){
        Excel::import(new ImportTeacher, $request->file('file')->store('files'));
        return redirect()->back();
    }

    public function exportPdf(Request $request)
    {
        $teachers = Teacher::with('course_students.courses.course_teachers.teachers')->orderBy('created_at', 'desc')->get();

        $data = [
            'title' => 'Welcome !!',
            'date' => date('d/m/Y'),
            'students' => $teachers
        ];

        if ($request->has('download')) {
            $pdf = PDF::loadView('teachers.pdf.index', $data);
            return $pdf->download('teachers.pdf');
        }

        return view('teachers.index', $data);
    }

}
