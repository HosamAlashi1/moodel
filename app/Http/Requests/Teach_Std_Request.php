<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Teach_Std_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'photo' => 'mimes:jpg,jpeg,png',
            'name_ar' => 'required|min:3|max:40',
            'age_ar' => 'required|numeric',
            'name_en' => 'required|min:3|max:40',
            'age_en' => 'required|numeric'
        ];
    }

    public function messages(){
        return [
            'required' => __('messages.This field is required'),
            'photo.mimes' => __('messages.the type of file chosen must be image'),
            'name_ar.min' => __('messages.the name length at least 3 character'),
            'name_ar.max' => __('messages.the name length at most 40 character'),
            'age_ar.numeric' => __('messages.the age must be number'),
            'name_en.min' => __('messages.the name length at least 3 character'),
            'name_en.max' => __('messages.the name length at most 40 character'),
            'age_en.numeric' => __('messages.the age must be number'),
        ];
    }
}
