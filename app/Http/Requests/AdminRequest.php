<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'photo' => 'mimes:jpg,jpeg,png',
            'name_ar' => 'required|min:3|max:40',
            'name_en' => 'required|min:3|max:40',
            'email' => 'required|email|unique:users,email,'.$this ->id,
            'user_name' => 'required|unique:users,user_name,'.$this ->id,
//            'password' => 'required|min:3',
            'roles' => 'required'
        ];
    }

    public function messages(){
        return [
            'required' => __('messages.This field is required'),
            'photo.mimes' => __('messages.the type of file chosen must be image'),
            'email.email' => __('messages.the email is invalid'),
            'email.unique' => __('messages.the email is already taken'),
            'user_name.unique' => __('messages.the user name is already taken'),
            'password.min' => __('messages.the password length at least 6 character'),
            'name_ar.min' => __('messages.the name length at least 3 character'),
            'name_ar.max' => __('messages.the name length at most 40 character'),
            'name_en.min' => __('messages.the name length at least 3 character'),
            'name_en.max' => __('messages.the name length at most 40 character'),
        ];
    }
}
