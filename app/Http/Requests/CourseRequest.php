<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        $courseId = $this->route('id');

        return [
            'name_ar' => 'required|min:3|max:40',
            'name_en' => 'required|min:3|max:40',
            'number' => 'required|unique:courses,number',
        ];
    }

    public function messages(){
        return [
            'required' => __('messages.This field is required'),
            'name_ar.min' => __('messages.the name length at least 3 character'),
            'name_ar.max' => __('messages.the name length at most 40 character'),
            'name_en.min' => __('messages.the name length at least 3 character'),
            'name_en.max' => __('messages.the name length at most 40 character'),
            'number.unique' => __('messages.the number is exist , must be unique'),
        ];
    }
}
