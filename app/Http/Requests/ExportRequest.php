<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'file' => 'required|mimes:csv,xlsx|max:8162', // max:8162  => maximum 8 mega
        ];
    }

    public function messages(){
        return [
            'required' => __('messages.This field is required'),
            'file.mimes' => __('messages.the type of file chosen must be csv , xlsx'),
            'max:8162' => __('messages.the maximum size of file is 8 mega'),
        ];
    }
}
