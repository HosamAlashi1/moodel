<?php

namespace App\DataTables;

use App\Models\Course;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\App;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class CourseDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', function ($course) {
                return view('courses.action', ['id' => $course->id]);
            })
            ->addColumn('name', function ($course) {
                return App::getLocale() == 'en' ? $course->name_en : $course->name_ar;
            })
            ->rawColumns(['action'])
            ->filterColumn('name', function ($query, $keyword) {
                $query->where(function ($query) use ($keyword) {
                    $query->where('name_en', 'like', '%' . $keyword . '%')
                        ->orWhere('name_ar', 'like', '%' . $keyword . '%');
                });
            });
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Course $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
            ->setTableId('course-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->orderBy(1)
            ->selectStyleSingle()
            ->language([
                'search' => __('messages.Search'),
                'lengthMenu' => __('messages.Show').' _MENU_ '.__('messages.Entries'),
                'zeroRecords' => __('messages.No matching records found'),
                'info' => __('messages.Showing').' _START_ '.__('messages.to').' _END_ '.__('messages.of').' _TOTAL_ '.__('messages.entries'),
                'infoEmpty' => __('messages.No records available'),
                'infoFiltered' => __('messages.filtered from').' _MAX_ '.__('messages.total records'),
                'paginate' => [
                    'first' => __('messages.First'),
                    'last' => __('messages.Last'),
                    'next' => __('messages.Next'),
                    'previous' => __('messages.Previous'),
                ],
            ])
            ->buttons([
                Button::make('excel'),
                Button::make('csv'),
                Button::make('pdf'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            ]);
    }



    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('name')->addClass('text-center pt-3')
                ->title(__('messages.Name')),
            Column::make('number')->addClass('text-center pt-3')
                ->title(__('messages.Number')),
            Column::computed('action')
                ->title(__('messages.Actions'))
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center pt-3')
                ->orderable(false)
                ->searchable(false),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Course_' . date('YmdHis');
    }
}
