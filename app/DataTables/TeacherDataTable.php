<?php

namespace App\DataTables;

use App\Models\Teacher;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\App;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TeacherDataTable extends DataTable
{
    /**
     * Build the DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', function ($teacher) {
                return view('teachers.action', ['id' => $teacher->id]);
            })
            ->addColumn('name', function ($teacher) {
                return App::getLocale() == 'en' ? $teacher->name_en : $teacher->name_ar;
            })
            ->addColumn('photo', function ($teacher) {
                return view('teachers.image', ['photo' => $teacher->photo]);
            })
            ->addColumn('age', function ($teacher) {
                return App::getLocale() == 'en' ? $teacher->age_en : $teacher->age_ar;
            })
            ->addColumn('courses', function ($teacher) {
                $html = '';
                foreach ($teacher->course_teachers as $course_teacher) {
                    if (App::getLocale() == 'en') {
                        $html .= $course_teacher->courses->name_en;
                    } else {
                        $html .= $course_teacher->courses->name_ar;
                    }
                    $html .= '<br>';
                }
                return $html;
            })
            ->rawColumns(['action', 'courses'])
            ->filterColumn('name', function ($query, $keyword) {
                $query->where(function ($query) use ($keyword) {
                    $query->where('name_en', 'like', '%' . $keyword . '%')
                        ->orWhere('name_ar', 'like', '%' . $keyword . '%');
                });
            })
            ->filterColumn('age', function ($query, $keyword) {
                // Filter the 'name' column based on the search keyword
                $query->where(function ($query) use ($keyword) {
                    $query->where('age_en', 'like', '%' . $keyword . '%')
                        ->orWhere('age_ar', 'like', '%' . $keyword . '%');
                });
            })->filterColumn('courses', function ($query, $keyword) {
                $query->whereHas('course_teachers', function ($query) use ($keyword) {
                    $query->whereHas('courses', function ($query) use ($keyword) {
                        $query->where('name_en', 'like', '%' . $keyword . '%')
                            ->orWhere('name_ar', 'like', '%' . $keyword . '%');
                    });
                });
            });
    }

    /**
     * Get the query source of dataTable.
     */
    public function query(Teacher $model): QueryBuilder
    {
        return $model->newQuery()->with('course_teachers.courses.course_students.students')
            ->orderBy('created_at', 'desc');
    }


    /**
     * Optional method if you want to use the html builder.
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('teacher-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    //->dom('Bfrtip')
                    ->orderBy(1)
                    ->selectStyleSingle()
                    ->language([
                        'search' => __('messages.Search'),
                        'lengthMenu' => __('messages.Show').' _MENU_ '.__('messages.Entries'),
                        'zeroRecords' => __('messages.No matching records found'),
                        'info' => __('messages.Showing').' _START_ '.__('messages.to').' _END_ '.__('messages.of').' _TOTAL_ '.__('messages.entries'),
                        'infoEmpty' => __('messages.No records available'),
                        'infoFiltered' => __('messages.filtered from').' _MAX_ '.__('messages.total records'),
                        'paginate' => [
                            'first' => __('messages.First'),
                            'last' => __('messages.Last'),
                            'next' => __('messages.Next'),
                            'previous' => __('messages.Previous'),
                        ],
                    ])
                    ->buttons([
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('pdf'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    ]);
    }

    /**
     * Get the dataTable columns definition.
     */
    public function getColumns(): array
    {
        return [
            Column::make('photo')
                ->addClass('text-center pt-3')
                ->title(__('messages.photo')),
            Column::make('name')->addClass('text-center pt-3')
                ->title(__('messages.Name'))
                ->searchable(true),
            Column::make('age')->addClass('text-center pt-3')
                ->title(__('messages.Age')),
            Column::make('courses')->addClass('text-center pt-3')
                ->title(__('messages.Courses')),
            Column::computed('action')
                ->title(__('messages.Actions'))
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center pt-3')
                ->orderable(false)
                ->searchable(false),
        ];
    }

    /**
     * Get the filename for export.
     */
    protected function filename(): string
    {
        return 'Teacher_' . date('YmdHis');
    }
}
