<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseStudent extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'course_student';
    protected $fillable = ['course_id','student_id'];


    public function courses(){
        return $this->belongsTo(Course::class,  'course_id', 'id');
    }

    public function students(){
        return $this->belongsTo(Student::class,  'student_id', 'id');
    }
}
