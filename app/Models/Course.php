<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable = ['name_ar','name_en','number','created_at','updated_at'];
    protected $hidden =['created_at','updated_at'];

    public function course_teachers(){
        return $this-> hasMany(CourseTeacher::class,'course_id', 'id');
    }

    public function course_students(){
        return $this-> hasMany(CourseStudent::class,'course_id', 'id');
    }

}
