<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseTeacher extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'course_teacher';
    protected $fillable = ['course_id','teacher_id'];

    public function courses(){
        return $this->belongsTo(Course::class,  'course_id', 'id');
    }

    public function teachers(){
        return $this->belongsTo(Teacher::class,  'teacher_id', 'id');
    }
}
