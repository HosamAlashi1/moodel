<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $fillable = ['name_ar','name_en','age_ar','age_en','photo','created_at','updated_at'];

    public function course_students(){
        return $this-> hasMany(CourseStudent::class,'student_id', 'id');
    }
}
