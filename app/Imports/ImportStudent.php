<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportStudent implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Student([
            'name_ar' => $row[0],
            'name_en' => $row[1],
            'age_ar' => $row[3],
            'age_en' => $row[4],
        ]);
    }
}
