<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Spatie\Permission\Models\Role;

class ImportAdmin implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $rolesName = json_decode($row[4], true); // Convert the string to an array

        $user = new User([
            'user_name' => $row[0],
            'name_ar' => $row[1],
            'name_en' => $row[2],
            'email' => $row[3],
            'roles_name' => $rolesName, // Assign the array value
            'password' => bcrypt($row[5]),
        ]);

        $user->save();

        // Assign roles
        $roles = Role::whereIn('name', $rolesName)->get();
        $user->assignRole($roles);

        return $user;
    }

}
