<?php

namespace App\Imports;

use App\Models\Teacher;
use Maatwebsite\Excel\Concerns\ToModel;

class ImportTeacher implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Teacher([
            'name_ar' => $row[0],
            'name_en' => $row[1],
            'age_ar' => $row[3],
            'age_en' => $row[4],
        ]);
    }
}
