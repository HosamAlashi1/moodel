<?php

namespace App\Exports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\FromArray;

class ExportStudent implements FromArray
{
    public function array():array
    {
        $list=[];
        $students = Student::all();
        foreach ($students as $student){
            $list[] = [$student->name_ar,$student->name_en,$student->age_ar,$student->age_en];
        }
        return $list;
    }
}
