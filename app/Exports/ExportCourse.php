<?php

namespace App\Exports;

use App\Models\Course;
use Maatwebsite\Excel\Concerns\FromArray;

class ExportCourse implements FromArray
{
    public function array():array
    {
        $list=[];
        $courses = Course::all();
        foreach ($courses as $course){
            $list[] = [$course->name_ar,$course->name_en,$course->number];
        }
        return $list;
    }
}
