<?php

namespace App\Exports;

use App\Models\Teacher;
use Maatwebsite\Excel\Concerns\FromArray;

class ExportTeacher implements FromArray
{
    public function array():array
    {
        $list=[];
        $teachers = Teacher::all();
        foreach ($teachers as $teacher){
            $list[] = [$teacher->name_ar,$teacher->name_en,$teacher->age_ar,$teacher->age_en];
        }
        return $list;
    }
}
