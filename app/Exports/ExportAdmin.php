<?php
namespace App\Exports;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;

class ExportAdmin implements FromArray {
    public function array():array
    {
        $list=[];
        $admins = User::all();
        foreach ($admins as $admin){
            $list[] = [$admin->user_name,$admin->name_ar,$admin->name_en,$admin->email,$admin->roles_name];
        }
        return $list;
    }
}

?>
