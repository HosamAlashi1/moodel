<?php

namespace App\Traits;

trait AdminTrait
{
    public function uploadImage($folder,$image){
        $image->store('/',$folder);
        $filename = $image->hashName();
        $path = 'images/' . $filename;
        return $path;
    }
}
