<?php

return [
    'ar' => 'باللغة العربية',
    'en' => 'باللغة الانجليزية',
    'Sign Out'=>'تسجيل الخروج',
    'Edit Profile'=>'تعديل الحساب',
    'Messages'=>'الرسائل',
    'Account Settings'=>'اعدادت الحساب',
    'Main' => 'الرئيسية',
    'Home' => 'الصفحة الرئيسية',
    'admin' => 'الأدمن',
    'general' => 'عام',
    'teachers' => 'المعلمين',
    'courses' => 'الكورسات',
    'students' => 'الطلاب',
    'Admins Table' => 'جدول الادمن',
    'Add new Admin' => 'اضافة ادمن جديد',
    'photo' => 'الصورة',
    'Name' => 'الاسم',
    'email' => ' البريد الإلكتروني',
    'Actions' => 'العمليات',
    'Delete' => 'حذف',
    'Create Admin' => 'إنشاءأدمن ',
    'Enter your Name' => 'أدخل اسمك',
    'Email' => 'البريد الإلكتروني',
    'Enter your email' => 'أدخل بريدك الإلكتروني',
    'Password' => 'كلمة المرور',
    'Enter your password' => 'أدخل كلمة المرور',
    'Create' => 'إنشاء',
    'Teachers Table' => 'جدول المعلمين',
    'Add new Teacher' => 'اضافة مدرس جديد',
    'Courses' => 'الكورسات',
    'Edit' => ' تعديل',
    'Age' => ' العمر',
    'Create Teacher' => 'إنشاء مدرس',
    'Enter Teacher Name' => ' أدخل اسم المعلم',
    'Enter Teacher age' => ' أدخل اسم المعلم',
    'Edit Teacher' => ' تعديل بيانات المعلم',
    'Number' => 'رقم الكورس',
    'Add Course for teacher' => ' إضافة كورس للمعلم',
    'Student-course Table' => ' جدول الكورسات المضافة لطالب',
    'Teacher-course Table' => ' جدول الكورسات المضافة للمعلم',
    'Add Course' => ' أضف الكورس',
    'Choose one' => ' اختر واحدة',
    'Add course for Teacher' => ' اضافة كورس للمعلم',
    'Success!' => ' نجحت العملية !',
    'Error!' => '  خطأ !',
    'Courses Table' => ' جدول الكورسات',
    'Add new Course' => 'أضف كورس جديد ',
    'Create Course' => ' أضافة كورس جديد',
    'Enter course Name' => 'أدخل اسم الكورس ',
    'Enter course number' => 'أدخل رقم الكورس',
    'Edit Course' => 'تعديل الكورس',
    'Enter Student Name' => 'أدخل اسم الطالب ',
    'Enter Student age' => 'أدخل عمر الطالب',
    'Create Student' => 'أضافة الطالب ',
    'Students Table' => 'جدول الطلاب',
    'Add new Student' => 'أضف طالب جديد ',
    'Edit Student' => ' تعديل بيانات الطالب',
    'Add Course for student' => 'اضافة كورس للطالب',
    'Created successfully' => ' تم الاضافة بنجاح',
    'There was an error try again' => ' حدث خطأ حاول مرة أخرى',
    'The Admin is not exist' => ' الادمن غير موجود',
    'deleted successfully' => ' تم الحذف بنجاح',
    'The Course is not exist' => ' الدورة غير موجودة',
    'updated successfully' => ' تم التحديث بنجاح',
    'The student is not exist' => ' الطالب غير موجود',
    'Added successfully' => ' تمت الاضافة بنجاح',
    'The teacher is not exist' => ' المعلم غير موجود',
    'This field is required' => 'هذا الحقل مطلوب',
    'the type of file chosen must be image' => 'يجب أن يكون نوع الملف المختار صورة',
    'the email is invalid'=>'البريد الإلكتروني غير صالح',
    'the password length at least 6 character'=>'طول كلمة المرور 6 أحرف على الأقل',
    'the name length at least 3 character'=>'يجب ألا يقل طول الاسم عن 3 أحرف',
    'the name length at most 40 character'=>'طول الاسم 40 حرفًا بحد أقصى ',
    'the number is exist , must be unique' => 'الرقم موجود ، يجب أن يكون فريدًا',
    'the age must be number' => 'يجب أن يكون العمر عددًا',
    'Name in arabic language'=>'الاسم باللغة العربية',
    'Enter your Name in arabic'=>'أدخل اسمك بالعربية',
    'Name in english language'=>'الاسم باللغة الإنجليزية',
    'Enter your Name in english' => 'أدخل اسمك باللغة الإنجليزية',
    'Enter course Name in arabic language'=>'أدخل اسم الدورة باللغة العربية',
    'Enter course Name in english language' => 'أدخل اسم الدورة باللغة الإنجليزية',

    'Age in arabic language' => 'العمر في اللغة العربية',
    'Age in english language' => 'العمر في اللغة الإنجليزية',
    'Enter Teacher age in arabic language' => 'أدخل عمر المعلم في اللغة العربية',
    'Enter Student age in arabic language' => 'أدخل عمر الطالب باللغة العربية',
    'Enter Teacher age in english language' => 'أدخل عمر المعلم في اللغة الإنجليزية',
    'Enter Student age in english language' => 'أدخل عمر الطالب في اللغة الإنجليزية',
    'Enter Teacher Name in arabic language' => 'أدخل اسم المعلم باللغة العربية',
    'Enter Student Name in arabic language' => 'أدخل اسم الطالب باللغة العربية',
    'Enter Teacher Name in english language' => 'أدخل اسم المعلم باللغة الإنجليزية',
    'Enter Student Name in english language' => 'أدخل اسم الطالب باللغة الإنجليزية',
    'roles users' => 'صلاحيات المستخدمين',
    'Roles Admin' => 'صلاحيات الادمن',
    'Edit Admin' => 'تحديث الادمن',
    'roles' => 'الصلاحيات',
    'Export Data' => 'تصدير اكسيل',
    'Export PDF' => 'تصدير PDF',
    'Import Data' => 'استيراد اكسيل',
    'the type of file chosen must be csv , xlsx' => 'يجب أن يكون امتداد الملف المختار csv، xlsx',
    'the maximum size of file is 8 mega' => 'الحجم الأقصى للملف هو 8 ميغا',
    'Search' => 'بحث',
    'Teacher' => 'المعلمين',
    'Student' => 'الطلاب',
    'Admins Roles' => 'صلاحيات الأدمن',
    'Courses Roles' => 'صلاحيات الكورسات',
    'Roles Permission' => 'صلاحيات الأذونات',
    'Teachers Roles' => 'صلاحيات المعلمين',
    'Students Roles' => 'صلاحيات الطلاب',
    'Display' => 'عرض',
    'Export Excel' => 'تصدير Excel',
    'Import Excel' => 'استيراد Excel',
    'Display Teacher Courses' => 'عرض كورسات المعلم',
    'Delete Teacher Courses' => 'حذف كورسات المعلم',
    'Create Teacher Courses' => 'اضافة كورسات للمعلم',
    'TEACHERS COUNT' => 'عدد المعلمين',
    'ADMINS COUNT' => 'عدد الادمن',
    'STUDENTS COUNT' => 'عدد الطلاب',
    'COURSES COUNT' => 'عدد الكورسات',
    'Teacher and Student count' => 'عدد المدرسين والطلاب',
    'print' => 'طباعة',
    'Role Name' => 'اسم الصلاحية',
    'Sure' => 'تأكيد',
    'show' => 'عرض',
    'add' => 'اضافة',
    'user name' => 'اسم المستخدم',
    'Enter user name' => 'ادخل اسم المستخدم',
    'the email is already taken' => 'البريد الإلكتروني مأخوذ بالفعل',
    'the user name is already taken' => 'اسم المستخدم مأخوذ بالفعل',
    'Show' => 'إظهار',
    'Entries' => 'إدخالات',
    'entries' => 'إدخالات',
    'Showing' => 'عرض',
    'of' => 'من',
    'to' => 'الى',
    'No matching records found' => 'لم يتم العثور على سجلات مطابقة',
    'total records' => 'إجمالي السجلات',
    'filtered from' => 'تمت تصفيته من',
    'No records available' => 'لا توجد سجلات متاحة',
    'are sure of the deleting process?' => 'هل أنت متأكد من عملية الحذف؟',
    'Delete Confirmation' => 'تأكيد الحذف',
    'Cancel' => 'الغاء',
    'Next' => 'التالي',
    'Previous' => 'السابق',
];
