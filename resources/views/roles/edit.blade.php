@extends('dashboard.layouts.master')
@section('css')
    <link href="{{URL::asset('dashboard/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <!--Internal  treeview -->
    <link href="{{URL::asset('dashboard/plugins/treeview/treeview-rtl.css')}}" rel="stylesheet" type="text/css" />
    <!-- Internal Select2 css -->
    <link href="{{URL::asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
    <!--Internal  Datetimepicker-slider css -->
    <link href="{{URL::asset('dashboard/plugins/amazeui-datetimepicker/css/amazeui.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{URL::asset('dashboard/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.css')}}" rel="stylesheet">
    <link href="{{URL::asset('dashboard/plugins/pickerjs/picker.min.css')}}" rel="stylesheet">
    <!-- Internal Spectrum-colorpicker css -->
    <link href="{{URL::asset('dashboard/plugins/spectrum-colorpicker/spectrum.css')}}" rel="stylesheet">
    <style>
        .checkbox-label {
            position: relative;
            display: inline-block;
            padding-left: 25px;
            font-size: 16px;
            cursor: pointer;
        }

        .checkbox-label .checkbox-custom {
            position: absolute;
            top: 2px;
            left: 0;
            width: 16px;
            height: 16px;
            border: 1px solid #ccc;
            background-color: #fff;
        }

        .checkbox-label .checkbox-custom::before {
            content: "";
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 8px;
            height: 8px;
            background-color: #0162e8;
            border-radius: 50%;
            opacity: 0;
            transition: opacity 0.2s ease-in-out;
        }

        .checkbox-label input[type="checkbox"]:checked + .checkbox-custom::before {
            opacity: 1;
        }

        .checkbox-label input[type="checkbox"] {
            display: none;
        }

        .checkbox-label:hover .checkbox-custom {
            border-color: #0162e8;
        }

        .checkbox-label:hover input[type="checkbox"]:checked + .checkbox-custom::before {
            background-color: #f95374;
        }

    </style>
@section('title')
تعديل الصلاحيات - مورا سوفت للادارة القانونية
@stop
@endsection
@section('page-header')
<!-- breadcrumb -->
<div class="breadcrumb-header justify-content-between">
    <div class="my-auto">
        <div class="d-flex">
            <h4 class="content-title mb-0 my-auto">الصلاحيات</h4><span class="text-muted mt-1 tx-13 mr-2 mb-0">/ تعديل
                الصلاحيات</span>
        </div>
    </div>
</div>
<!-- breadcrumb -->
@endsection
@section('content')

@if (count($errors) > 0)
<div class="alert alert-danger">
    <button aria-label="Close" class="close" data-dismiss="alert" type="button">
        <span aria-hidden="true">&times;</span>
    </button>
    <strong>خطا</strong>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


{!! Form::model($role, ['method' => 'post','route' => ['roles.update', $role->id]]) !!}
<!-- row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="main-content-label mg-b-5">
            <div class="col-xs-7 col-sm-7 col-md-7">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <p>{{__('messages.Role Name')}} :</p>
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-main-primary">{{__('messages.Sure')}}</button>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                </div>

            </div>
        </div>
        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">{{__('messages.Admins Roles')}}</h4>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0 text-md-nowrap">
                        <thead>
                        <tr>
                            <th>{{__('messages.Display')}}</th>
                            <th>{{__('messages.Edit')}}</th>
                            <th>{{__('messages.Delete')}}</th>
                            <th>{{__('messages.Create')}}</th>
                            <th>{{__('messages.Export PDF')}}</th>
                            <th>{{__('messages.Import Excel')}}</th>
                            <th>{{__('messages.Export Excel')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permission->take(7) as $value)
                                <td>
                                    <label class="checkbox-label">
                                        {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                        <div class="main-toggle main-toggle-success{{ in_array($value->id, $rolePermissions) ? ' on' : '' }}">
                                            <span></span>
                                        </div>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">{{__('messages.Roles Permission')}}</h4>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0 text-md-nowrap">
                        <thead>
                        <tr>
                            <th>{{__('messages.Display')}}</th>
                            <th>{{__('messages.Create')}}</th>
                            <th>{{__('messages.Edit')}}</th>
                            <th>{{__('messages.Delete')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permission->skip(7)->take(4) as $value)
                                <td>
                                    <label class="checkbox-label">
                                        {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                        <div class="main-toggle main-toggle-success{{ in_array($value->id, $rolePermissions) ? ' on' : '' }}">
                                            <span></span>
                                        </div>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">{{__('messages.Courses Roles')}}</h4>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0 text-md-nowrap">
                        <thead>
                        <tr>
                            <th>{{__('messages.Display')}}</th>
                            <th>{{__('messages.Create')}}</th>
                            <th>{{__('messages.Edit')}}</th>
                            <th>{{__('messages.Delete')}}</th>
                            <th>{{__('messages.Export PDF')}}</th>
                            <th>{{__('messages.Import Excel')}}</th>
                            <th>{{__('messages.Export Excel')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permission->skip(11)->take(7) as $value)
                                <td>
                                    <label class="checkbox-label">
                                        {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                        <div class="main-toggle main-toggle-success{{ in_array($value->id, $rolePermissions) ? ' on' : '' }}">
                                            <span></span>
                                        </div>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">{{__('messages.Teachers Roles')}}</h4>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0 text-md-nowrap">
                        <thead>
                        <tr>
                            <th>{{__('messages.Display')}}</th>
                            <th>{{__('messages.Edit')}}</th>
                            <th>{{__('messages.Delete')}}</th>
                            <th>{{__('messages.Create')}}</th>
                            <th>{{__('messages.Display Teacher Courses')}}</th>
                            <th>{{__('messages.Create Teacher Courses')}}</th>
                            <th>{{__('messages.Delete Teacher Courses')}}</th>
                            <th>{{__('messages.Export PDF')}}</th>
                            <th>{{__('messages.Import Excel')}}</th>
                            <th>{{__('messages.Export Excel')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permission->skip(18)->take(10) as $value)
                                <td>
                                    <label class="checkbox-label">
                                        {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                        <div class="main-toggle main-toggle-success{{ in_array($value->id, $rolePermissions) ? ' on' : '' }}">
                                            <span></span>
                                        </div>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>


        </div>

        <div class="card">
            <div class="card-header pb-0">
                <div class="d-flex justify-content-between">
                    <h4 class="card-title mg-b-0">{{__('messages.Students Roles')}}</h4>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover mb-0 text-md-nowrap">
                        <thead>
                        <tr>
                            <th>{{__('messages.Display')}}</th>
                            <th>{{__('messages.Edit')}}</th>
                            <th>{{__('messages.Delete')}}</th>
                            <th>{{__('messages.Create')}}</th>
                            <th>{{__('messages.Display Teacher Courses')}}</th>
                            <th>{{__('messages.Create Teacher Courses')}}</th>
                            <th>{{__('messages.Delete Teacher Courses')}}</th>
                            <th>{{__('messages.Export PDF')}}</th>
                            <th>{{__('messages.Import Excel')}}</th>
                            <th>{{__('messages.Export Excel')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($permission->skip(28)->take(10) as $value)
                                <td>
                                    <label class="checkbox-label">
                                        {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                        <div class="main-toggle main-toggle-success{{ in_array($value->id, $rolePermissions) ? ' on' : '' }}">
                                            <span></span>
                                        </div>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection
@section('script')
    <script src="{{URL::asset('dashboard/plugins/treeview/treeview.js')}}"></script>
    <script src="{{URL::asset('dashboard/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal  jquery.maskedinput js -->
    <script src="{{URL::asset('dashboard/plugins/jquery.maskedinput/jquery.maskedinput.js')}}"></script>
    <!--Internal  spectrum-colorpicker js -->
    <script src="{{URL::asset('dashboard/plugins/spectrum-colorpicker/spectrum.js')}}"></script>
    <!-- Internal Select2.min js -->
    <script src="{{URL::asset('dashboard/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal Ion.rangeSlider.min js -->
    <script src="{{URL::asset('dashboard/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!--Internal  jquery-simple-datetimepicker js -->
    <script src="{{URL::asset('dashboard/plugins/amazeui-datetimepicker/js/amazeui.datetimepicker.min.js')}}"></script>
    <!-- Ionicons js -->
    <script src="{{URL::asset('dashboard/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.js')}}"></script>
    <!--Internal  pickerjs js -->
    <script src="{{URL::asset('dashboard/plugins/pickerjs/picker.min.js')}}"></script>
    <!-- Internal form-elements js -->
    <script src="{{URL::asset('dashboard/js/form-elements.js')}}"></script>
@endsection
