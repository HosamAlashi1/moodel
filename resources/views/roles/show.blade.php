@extends('dashboard.layouts.master')
@section('css')
    <link href="{{URL::asset('dashboard/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <!--Internal  treeview -->
    <link href="{{URL::asset('dashboard/plugins/treeview/treeview-rtl.css')}}" rel="stylesheet" type="text/css" />
    <!-- Internal Select2 css -->
    <link href="{{URL::asset('dashboard/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
    <!--Internal  Datetimepicker-slider css -->
    <link href="{{URL::asset('dashboard/plugins/amazeui-datetimepicker/css/amazeui.datetimepicker.css')}}" rel="stylesheet">
    <link href="{{URL::asset('dashboard/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.css')}}" rel="stylesheet">
    <link href="{{URL::asset('dashboard/plugins/pickerjs/picker.min.css')}}" rel="stylesheet">
    <!-- Internal Spectrum-colorpicker css -->
    <link href="{{URL::asset('dashboard/plugins/spectrum-colorpicker/spectrum.css')}}" rel="stylesheet">
    @section('title')
        تعديل الصلاحيات - مورا سوفت للادارة القانونية
    @stop
@endsection

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <button aria-label="Close" class="close" data-dismiss="alert" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>خطا</strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <!-- row -->
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{__('messages.Admins Roles')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0 text-md-nowrap">
                            <thead>
                            <tr style="text-align: center;">
                                <th>{{__('messages.Display')}}</th>
                                <th>{{__('messages.Edit')}}</th>
                                <th>{{__('messages.Delete')}}</th>
                                <th>{{__('messages.Create')}}</th>
                                <th>{{__('messages.Export PDF')}}</th>
                                <th>{{__('messages.Import Excel')}}</th>
                                <th>{{__('messages.Export Excel')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($permission->take(7) as $value)
                                    <td style="text-align: center;">
                                        <label>
                                            <i class="icon {{ in_array($value->id, $rolePermissions) ? 'ion-ios-checkmark' : 'icon ion-ios-close' }}"
                                               style="color: {{ in_array($value->id, $rolePermissions) ? '#00eb1b' : 'red' }}; font-size: 50px;"></i>
                                        </label>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{__('messages.Roles Permission')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0 text-md-nowrap">
                            <thead>
                            <tr style="text-align: center;">
                                <th>{{__('messages.Display')}}</th>
                                <th>{{__('messages.Create')}}</th>
                                <th>{{__('messages.Edit')}}</th>
                                <th>{{__('messages.Delete')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($permission->skip(7)->take(4) as $value)
                                    <td style="text-align: center;">
                                        <label>
                                            <i class="icon {{ in_array($value->id, $rolePermissions) ? 'ion-ios-checkmark' : 'icon ion-ios-close' }}"
                                               style="color: {{ in_array($value->id, $rolePermissions) ? '#00eb1b' : 'red' }}; font-size: 50px;"></i>
                                        </label>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{__('messages.Courses Roles')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0 text-md-nowrap">
                            <thead>
                            <tr style="text-align: center;">
                                <th>{{__('messages.Display')}}</th>
                                <th>{{__('messages.Create')}}</th>
                                <th>{{__('messages.Edit')}}</th>
                                <th>{{__('messages.Delete')}}</th>
                                <th>{{__('messages.Export PDF')}}</th>
                                <th>{{__('messages.Import Excel')}}</th>
                                <th>{{__('messages.Export Excel')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($permission->skip(11)->take(7) as $value)
                                    <td style="text-align: center;">
                                        <label>
                                            <i class="icon {{ in_array($value->id, $rolePermissions) ? 'ion-ios-checkmark' : 'icon ion-ios-close' }}"
                                               style="color: {{ in_array($value->id, $rolePermissions) ? '#00eb1b' : 'red' }}; font-size: 50px;"></i>
                                        </label>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{__('messages.Teachers Roles')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0 text-md-nowrap">
                            <thead>
                            <tr style="text-align: center;">
                                <th>{{__('messages.Display')}}</th>
                                <th>{{__('messages.Edit')}}</th>
                                <th>{{__('messages.Delete')}}</th>
                                <th>{{__('messages.Create')}}</th>
                                <th>{{__('messages.Display Teacher Courses')}}</th>
                                <th>{{__('messages.Create Teacher Courses')}}</th>
                                <th>{{__('messages.Delete Teacher Courses')}}</th>
                                <th>{{__('messages.Export PDF')}}</th>
                                <th>{{__('messages.Import Excel')}}</th>
                                <th>{{__('messages.Export Excel')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr style="text-align: center;">
                                @foreach($permission->skip(18)->take(10) as $value)
                                    <td style="text-align: center;">
                                        <label>
                                            <i class="icon {{ in_array($value->id, $rolePermissions) ? 'ion-ios-checkmark' : 'icon ion-ios-close' }}"
                                               style="color: {{ in_array($value->id, $rolePermissions) ? '#00eb1b' : 'red' }}; font-size: 50px;"></i>
                                        </label>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>

            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{__('messages.Students Roles')}}</h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0 text-md-nowrap">
                            <thead>
                            <tr style="text-align: center;">
                                <th>{{__('messages.Display')}}</th>
                                <th>{{__('messages.Edit')}}</th>
                                <th>{{__('messages.Delete')}}</th>
                                <th>{{__('messages.Create')}}</th>
                                <th>{{__('messages.Display Teacher Courses')}}</th>
                                <th>{{__('messages.Create Teacher Courses')}}</th>
                                <th>{{__('messages.Delete Teacher Courses')}}</th>
                                <th>{{__('messages.Export PDF')}}</th>
                                <th>{{__('messages.Import Excel')}}</th>
                                <th>{{__('messages.Export Excel')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($permission->skip(28)->take(10) as $value)
                                    <td style="text-align: center;">
                                        <label>
                                            <i class="icon {{ in_array($value->id, $rolePermissions) ? 'ion-ios-checkmark' : 'icon ion-ios-close' }}"
                                               style="color: {{ in_array($value->id, $rolePermissions) ? '#00eb1b' : 'red' }}; font-size: 50px;"></i>
                                        </label>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection
@section('script')
    <script src="{{URL::asset('dashboard/plugins/treeview/treeview.js')}}"></script>
    <script src="{{URL::asset('dashboard/plugins/jquery-ui/ui/widgets/datepicker.js')}}"></script>
    <!--Internal  jquery.maskedinput js -->
    <script src="{{URL::asset('dashboard/plugins/jquery.maskedinput/jquery.maskedinput.js')}}"></script>
    <!--Internal  spectrum-colorpicker js -->
    <script src="{{URL::asset('dashboard/plugins/spectrum-colorpicker/spectrum.js')}}"></script>
    <!-- Internal Select2.min js -->
    <script src="{{URL::asset('dashboard/plugins/select2/js/select2.min.js')}}"></script>
    <!--Internal Ion.rangeSlider.min js -->
    <script src="{{URL::asset('dashboard/plugins/ion-rangeslider/js/ion.rangeSlider.min.js')}}"></script>
    <!--Internal  jquery-simple-datetimepicker js -->
    <script src="{{URL::asset('dashboard/plugins/amazeui-datetimepicker/js/amazeui.datetimepicker.min.js')}}"></script>
    <!-- Ionicons js -->
    <script src="{{URL::asset('dashboard/plugins/jquery-simple-datetimepicker/jquery.simple-dtpicker.js')}}"></script>
    <!--Internal  pickerjs js -->
    <script src="{{URL::asset('dashboard/plugins/pickerjs/picker.min.js')}}"></script>
    <!-- Internal form-elements js -->
    <script src="{{URL::asset('dashboard/js/form-elements.js')}}"></script>
@endsection
