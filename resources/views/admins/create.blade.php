@extends('dashboard.layouts.master')

@section('title', 'Create Admin')

@section('content')
    <form action="{{route('admin.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            @include('dashboard.layouts.alerts.error')
            @include('dashboard.layouts.alerts.success')
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="main-content-label mg-b-5">
                            {{__('messages.Create Admin')}}
                        </div>
                        <div class="pd-30 pd-sm-40 bg-gray-200">

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.user name')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{ old('user_name') }}" class="form-control" name="user_name" placeholder="{{__('messages.Enter user name')}}" type="text">
                                </div>
                                @error('user_name')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Name in arabic language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{ old('name_ar') }}" class="form-control" name="name_ar" placeholder="{{__('messages.Enter your Name in arabic')}}" type="text">
                                </div>
                                @error('name_ar')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Name in english language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{ old('name_en') }}" class="form-control" name="name_en" placeholder="{{__('messages.Enter your Name in english')}}" type="text">
                                </div>
                                @error('name_en')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Email')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{ old('email') }}" class="form-control" name="email" placeholder="{{__('messages.Enter your email')}}" type="email">
                                </div>
                                @error('email')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Password')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input name="password" class="form-control" placeholder="{{__('messages.Enter your password')}}" type="password">
                                </div>
                                @error('password')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Roles Admin')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    {!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
                                </div>
                                @error('roles')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.photo')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input name="photo" class="form-control custom-file-input" type="file">
                                </div>
                                @error('photo')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">{{__('messages.Create')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
