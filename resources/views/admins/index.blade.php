@extends('dashboard.layouts.master')

@section('title', 'Admins')

@section('css')
    <link href="https://cdn.datatables.net/1.13.5/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        @media print {
            #print_Button {
                display: none;
            }
            button {
                display: none;
            }
            .btn {
                display: none;
            }
            input {
                display: none;
            }
            .hide {
                display: none;
            }
        }
        table.dataTable tbody td {
            text-align: center;
        }
        .text-center {
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header pb-0">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title mg-b-0">{{__('messages.Admins Table')}}</h4>
                    </div>
                    <br>
                    <div class="col-sm-12">
                        <div class="d-flex justify-content-between align-items-center mt-4">
                            <form action="{{ route('admin.import') }}" method="POST" enctype="multipart/form-data">
                                <input type="file" name="file" class="form-control">
                                @csrf
                                @error('file')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <div class="mt-4">
                                    <a class="btn btn-danger" href="{{ route('admin.payment') }}">buy</a>
                                    @can('create admin')
                                       <a href="{{ route('admin.create') }}" type="button" class="btn btn-primary">{{ __('messages.Add new Admin') }}</a>
                                    @endcan
                                    @can('export-pdf admin')
                                        <a href="{{ route('admin.export-pdf',['download'=>'pdf']) }}" type="button" class="btn btn-primary">{{ __('messages.Export PDF') }}</a>
                                    @endcan
                                    @can('export-excel admin')
                                        <a class="btn btn-primary" href="{{ route('admin.export') }}">{{ __('messages.Export Data') }}</a>
                                    @endcan
                                    @can('import-excel admin')
                                        <button class="btn btn-primary">{{ __('messages.Import Data') }}</button>
                                    @endcan
                                    <button class="btn btn-danger" id="print_Button" onclick="printDiv()">{{__('messages.print')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body" id="print">
                    <div class="table-responsive">
                        {{ $dataTable->table() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modaldemo8" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-content-demo">
                <div class="modal-header">
                    <h6 class="modal-title">{{ trans('admins.dele') }}</h6><button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <p>{{ trans('admins.aresure') }}</p><br>
                    <input class="form-control" name="usernamed" id="usernamed" type="text" readonly="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('admins.close') }}</button>
                    <button type="submit" class="btn btn-danger" id="dletet">{{ trans('admins.save') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!--Internal Chart.bundle js-->
    <script src="{{ URL::asset('dashboard/plugins/chart.js/Chart.bundle.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>

    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

    <script>
        function printDiv() {
            var printContents = document.getElementById('print').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
            location.reload();
        }

        $(document).on('click', '#DeleteCategory', function(e) {
            e.preventDefault();
            $('#usernamed').val($(this).data('namee'));
            var id_admin = $(this).data('id');
            // jQuery.noConflict();
            $('#modaldemo8').modal('show');
            aaaa(id_admin);
        });
        function aaaa(id) {
            $(document).off("click", "#dletet").on("click", "#dletet", function (e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'GET',
                    url: '{{ url("admin/delete") }}/' + id,
                    data: '',
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        $('#error_message').html("");
                        $('#error_message').addClass("alert alert-danger");
                        $('#error_message').text(response.message);
                        // jQuery.noConflict();
                        $('#modaldemo8').modal('hide');
                        table.ajax.reload();
                    }
                });
            });
        };

    </script>
@endsection
