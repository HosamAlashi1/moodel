@can('edit admin')
    <a href="{{ route('admin.edit', $admin->id) }}" type="button" class="btn btn-primary">{{ __('messages.Edit') }}</a>
@endcan

@can('delete admin')
    <button type="button" class="btn btn-danger" id="DeleteCategory" data-id="{{ $admin->id }}" data-namee="{{ $admin->name }}">
        {{ __('messages.Delete') }}
    </button>
@endcan
