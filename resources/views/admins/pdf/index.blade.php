<!DOCTYPE html>
<html>
<head>
    <title>Generate PDF File</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>{{ $title }}</h1>
<p>{{ $date }}</p>
<div class="container">
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Roles</th>
        </tr>
        @foreach($admins as $admin)
            <tr>
                <th>{{ $admin->id }}</th>
                <td>{{ $admin->name_en }}</td>
                <td>{{ $admin->email }}</td>
                <td class="pt-3">
                    @if (!empty($admin->getRoleNames()))
                        @foreach ($admin->getRoleNames() as $role)
                            <label class=" badge-success">{{ $role }}</label>
                        @endforeach
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>
</body>
</html>
