@extends('dashboard.layouts.master')

@section('title', 'Update Teacher')

@section('content')
    <form action="{{route('teacher.update',$teacher->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            @include('dashboard.layouts.alerts.error')
            @include('dashboard.layouts.alerts.success')
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="main-content-label mg-b-5">
                            {{__('messages.Edit Teacher')}}
                        </div>

                        <div class="pd-30 pd-sm-40 bg-gray-200">
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <div class="main-img-user"><img alt="" src="{{ $teacher->photo != '' ? asset($teacher->photo) : asset('dashboard/img/users/6.jpg') }}" class=""></div>
                                </div>
                                <br><br>
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Name in arabic language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{ $teacher->name_ar }}" class="form-control" name="name_ar" placeholder="{{__('messages.Enter Teacher Name in arabic language')}}" type="text">
                                </div>
                                @error('name_ar')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Name in english language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{ $teacher->name_en }}" class="form-control" name="name_en" placeholder="{{__('messages.Enter Teacher Name in english language')}}" type="text">
                                </div>
                                @error('name_en')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>


                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Age in arabic language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{$teacher->age_ar}}" class="form-control" name="age_ar" placeholder="{{__('messages.Enter Teacher age in arabic language')}}" type="number">
                                </div>
                                @error('age_ar')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Age in english language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{$teacher->age_en}}" class="form-control" name="age_en" placeholder="{{__('messages.Enter Teacher age in english language')}}" type="number">
                                </div>
                                @error('age_en')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.photo')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input name="photo" class="form-control custom-file-input" type="file">
                                </div>
                                @error('photo')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">{{__('messages.Edit')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </form>
@endsection
