@extends('dashboard.layouts.master')

@section('title', 'Teacher details')

@section('content')
    <div class="row">
        @include('dashboard.layouts.alerts.error')
        @include('dashboard.layouts.alerts.success')
        <div class="col-lg-12 col-md-12">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title mg-b-0">{{__('messages.Teacher-course Table')}}</h4>
                        </div>
                        <br>
                        @can('create teacher courses')
                            <a href="{{route('teacher.course.create',$id)}}" type="button" class="btn btn-primary">{{__('messages.Add Course for teacher')}}</a>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0 text-md-nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('messages.Name')}}</th>
                                    <th>{{__('messages.Number')}}</th>
                                    <th>{{__('messages.Actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                    <tr>
                                        @if(App::getLocale() == 'en')
                                            <td class="pt-3">{{$course->courses->name_en}}</td>
                                        @else
                                            <td class="pt-3">{{$course->courses->name_ar}}</td>
                                        @endif
                                        <td class="pt-3">{{$course->courses->number}}</td>
                                        <td class="pt-3">
                                            @can('delete teacher courses')
                                                <a href="{{route('student.course.delete',[$course->courses->id,$id])}}" type="button" class="btn btn-danger">{{__('messages.Delete')}}</a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {{ $courses->links() }}
            </div>
        </div>
    </div>
@endsection

