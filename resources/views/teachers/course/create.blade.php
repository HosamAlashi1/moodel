@extends('dashboard.layouts.master')

@section('title', 'Add Course-Teacher')

@section('content')
    <form action="{{route('teacher.course.store',$teacher_id)}}" method="post">
        @csrf
        <div class="row">
            @include('dashboard.layouts.alerts.error')
            @include('dashboard.layouts.alerts.success')
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="main-content-label mg-b-5">
                            {{__('messages.Add course for Teacher')}}
                        </div>
                        <div class="pd-30 pd-sm-40 bg-gray-200">
                            <div class="col-lg-4">
                                <div class="row row-xs align-items-center mg-b-20">
                                <select name="course_id" class="form-control select2-no-search">
                                    <option label="{{__('messages.Choose one')}}" selected disabled>
                                    </option>
                                    @if ($courses && $courses->count() > 0)
                                        @foreach ($courses as $course)
                                            <option value="{{ $course->id }}">
                                                @if(App::getLocale() == 'en')
                                                    {{$course->name_en}}
                                                @else
                                                    {{$course->name_ar}}
                                                @endif
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div><!-- col-4 -->
                            </div>
                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">{{__('messages.Add Course')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
