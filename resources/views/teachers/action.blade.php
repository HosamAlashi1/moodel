@can('edit teacher')
    <a href="{{route('teacher.edit',$id)}}" type="button" class="btn btn-primary">{{__('messages.Edit')}}</a>
@endcan
@can('delete teacher')
    <a href="{{route('teacher.delete',$id)}}" type="button" class="btn btn-danger">{{__('messages.Delete')}}</a>
@endcan
@can('display teacher courses')
    <a href="{{route('teacher.course.index',$id)}}" type="button" class="btn btn-success">{{__('messages.Courses')}}</a>
@endcan
