@extends('dashboard.layouts.master')

@section('title', 'Courses')
@section('css')
    <link href="https://cdn.datatables.net/1.13.5/css/jquery.dataTables.min.css" rel="stylesheet">
    <style>
        @media print {
            #print_Button {
                display: none;
            }
            button {
                display: none;
            }
            .btn {
                display: none;
            }
            input {
                display: none;
            }
            .hide {
                display: none;
            }
        }
        table.dataTable tbody td {
            text-align: center;
        }
        .text-center{
            text-align: center;
        }


    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <div class="d-flex justify-content-between">
                            <h4 class="card-title mg-b-0">{{__('messages.Courses Table')}}</h4>
                        </div>
                        <br>
                        <div class="col-sm-12">
                            <div class="d-flex justify-content-between align-items-center mt-4">
                                <form action="{{ route('course.import') }}" method="POST" enctype="multipart/form-data">
                                    <input type="file" name="file" class="form-control">
                                    @error('file')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    @csrf
                                    <div class="mt-4">
                                        @can('create course')
                                            <a href="{{route('course.create')}}" type="button" class="btn btn-primary">{{__('messages.Add new Course')}}</a>
                                        @endcan
                                        @can('export-pdf course')
                                            <a href="{{ route('course.export-pdf',['download'=>'pdf']) }}" type="button" class="btn btn-primary">{{ __('messages.Export PDF') }}</a>
                                        @endcan
                                        @can('export-excel course')
                                            <a class="btn btn-primary" href="{{ route('course.export') }}">{{ __('messages.Export Data') }}</a>
                                        @endcan
                                        @can('import-excel course')
                                            <button class="btn btn-primary">{{ __('messages.Import Data') }}</button>
                                        @endcan
                                            <button class="btn btn-danger" id="print_Button" onclick="printDiv()">
                                                {{--                                            <i class="mdi mdi-printer"></i> --}}{{__('messages.print')}} </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body" id="print">
                        <div class="table-responsive">
                            {{ $dataTable->table() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!--Internal  Chart.bundle js -->
    <script src="{{ URL::asset('assets/plugins/chart.js/Chart.bundle.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>


    <script type="text/javascript">
        function printDiv() {
            var printContents = document.getElementById('print').innerHTML;
            var originalContents = document.body.innerHTML;
            document.body.innerHTML = printContents;
            window.print();
            document.body.innerHTML = originalContents;
            location.reload();
        }
    </script>

    {{ $dataTable->scripts(attributes: ['type' => 'module']) }}

@endsection
