@can('edit course')
    <a href="{{route('course.edit',$id)}}" type="button" class="btn btn-primary">{{__('messages.Edit')}}</a>
@endcan
@can('delete course')
    <a href="{{route('course.delete',$id)}}" type="button" class="btn btn-danger">{{__('messages.Delete')}}</a>
@endcan
