@extends('dashboard.layouts.master')

@section('title', 'Edit Course')

@section('content')
    <form action="{{route('course.update',$course->id)}}" method="post">
        @csrf
        <div class="row">
            @include('dashboard.layouts.alerts.error')
            @include('dashboard.layouts.alerts.success')
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="main-content-label mg-b-5">
                            {{__('messages.Edit Course')}}
                        </div>
                        <div class="pd-30 pd-sm-40 bg-gray-200">
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Name in arabic language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{$course->name_ar}}" class="form-control" name="name_ar" placeholder="{{__('messages.Enter course Name in arabic language')}}" type="text">
                                </div>
                                @error('name_ar')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Name in english language')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{$course->name_en}}" class="form-control" name="name_en" placeholder="{{__('messages.Enter course Name in english language')}}" type="text">
                                </div>
                                @error('name_en')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="row row-xs align-items-center mg-b-20">
                                <div class="col-md-4">
                                    <label class="form-label mg-b-0">{{__('messages.Number')}}</label>
                                </div>
                                <div class="col-md-8 mg-t-5 mg-md-t-0">
                                    <input value="{{$course->number}}" class="form-control" name="number" placeholder="{{__('messages.Enter course number')}}" type="text">
                                </div>
                                @error('number')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <button class="btn btn-main-primary pd-x-30 mg-r-5 mg-t-5">{{__('messages.Edit')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
