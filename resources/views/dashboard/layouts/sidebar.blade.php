<div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
<div class="sticky">
    <aside class="app-sidebar sidebar-scroll">
{{--        <div class="main-sidebar-header active">--}}
{{--            <a class="desktop-logo logo-light active" href="index.html"><img--}}
{{--                    src="{{ asset('dashboard/img/brand/logo-light.png') }}" class="main-logo" alt="logo"></a>--}}
{{--            <a class="desktop-logo logo-dark active" href="index.html"><img--}}
{{--                    src="{{ asset('dashboard/img/brand/logo-light.png') }}" class="main-logo" alt="logo"></a>--}}
{{--            <a class="logo-icon mobile-logo icon-light active" href="index.html"><img--}}
{{--                    src="{{ asset('dashboard/img/brand/logo-light.png') }}" alt="logo"></a>--}}
{{--            <a class="logo-icon mobile-logo icon-dark active" href="index.html"><img--}}
{{--                    src="{{ asset('dashboard/img/brand/logo-light.png') }}" alt="logo"></a>--}}
{{--        </div>--}}
        <div class="main-sidemenu">
            <div class="app-sidebar__user clearfix">
                <div class="dropdown user-pro-body">
                    <div class="main-img-user avatar-xl">
                        <img alt="user-img" src="{{ auth()->user()->photo != '' ? asset(auth()->user()->photo) : asset('dashboard/img/users/6.jpg') }}"><span
                            class="avatar-status profile-status bg-green"></span>
                    </div>
                    <div class="user-info">
                        <h4 class="fw-semibold mt-3 mb-0">
                            @if(App::getLocale() == 'en')
                                {{ auth()->user()->name_en }}
                            @else
                                {{ auth()->user()->name_ar }}
                            @endif</h4>
                        <span class="mb-0 text-muted">{{ auth()->user()->email }}</span>
                    </div>
                </div>
            </div>
            <div class="slide-left disabled" id="slide-left"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191"
                    width="24" height="24" viewBox="0 0 24 24">
                    <path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z" />
                </svg></div>
            <ul class="side-menu">
                <li class="side-item side-item-category">{{ __('messages.Main') }}</li>
                <li class="slide">
                    <a class="side-menu__item" href="{{ route('main') }}"><svg xmlns="http://www.w3.org/2000/svg"
                            class="side-menu__icon" viewBox="0 0 24 24">
                            <svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M19 5H5v14h14V5zM9 17H7v-7h2v7zm4 0h-2V7h2v10zm4 0h-2v-4h2v4z" opacity=".3"/><path d="M3 5v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2zm2 0h14v14H5V5zm2 5h2v7H7zm4-3h2v10h-2zm4 6h2v4h-2z"/></svg><span class="side-menu__label">{{ __('messages.Home') }}</span></a>
                </li>
                <ul class="side-menu">
                    @can('display admin')
                    <li class="slide">
                            <a class="side-menu__item" href="{{ route('admin.index') }}"><svg xmlns="http://www.w3.org/2000/svg"
                               class="side-menu__icon" viewBox="0 0 24 24">
                                <path d="M0 0h24v24H0V0z" fill="none" />
                                <path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3" />
                                <path
                                    d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z" />
                            </svg><span class="side-menu__label">{{__('messages.admin')}}</span></a>
                    </li>
                    @endcan
                    @can('display roles')
                        <li class="slide">
                            <a class="side-menu__item" href="{{ route('roles.index') }}"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                              class="side-menu__icon" viewBox="0 0 24 24">
                                    <path d="M0 0h24v24H0V0z" fill="none" />
                                    <path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3" />
                                    <path
                                        d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z" />
                                </svg><span class="side-menu__label">{{__('messages.roles users')}}</span></a>
                        </li>
                    @endcan
                    <ul class="side-menu">
                        <li class="side-item side-item-category">{{__('messages.general')}}</li>
                        @can('display teacher')
                        <li class="slide">
                                <a class="side-menu__item" href="{{ route('teacher.index') }}"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                       class="side-menu__icon" viewBox="0 0 24 24">
                                    <path d="M0 0h24v24H0V0z" fill="none" />
                                    <path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3" />
                                    <path
                                        d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z" />
                                </svg><span class="side-menu__label">{{__('messages.teachers')}}</span></a>
                        </li>
                    @endcan
                    @can('display course')
                            <li class="slide">
                                    <a class="side-menu__item" href="{{ route('course.index') }}"><svg xmlns="http://www.w3.org/2000/svg" class="side-menu__icon" viewBox="0 0 24 24"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M13 4H6v16h12V9h-5V4zm3 14H8v-2h8v2zm0-6v2H8v-2h8z" opacity=".3"/><path d="M8 16h8v2H8zm0-4h8v2H8zm6-10H6c-1.1 0-2 .9-2 2v16c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11z"/><span class="side-menu__label">{{__('messages.courses')}}</span></a>
                            </li>
                    @endcan
                    @can('display student')

                                <li class="slide">
                                        <a class="side-menu__item" href="{{ route('student.index') }}"><svg xmlns="http://www.w3.org/2000/svg"
                                                                                               class="side-menu__icon" viewBox="0 0 24 24">
                                            <path d="M0 0h24v24H0V0z" fill="none" />
                                            <path d="M5 5h4v6H5zm10 8h4v6h-4zM5 17h4v2H5zM15 5h4v2h-4z" opacity=".3" />
                                            <path
                                                d="M3 13h8V3H3v10zm2-8h4v6H5V5zm8 16h8V11h-8v10zm2-8h4v6h-4v-6zM13 3v6h8V3h-8zm6 4h-4V5h4v2zM3 21h8v-6H3v6zm2-4h4v2H5v-2z" />
                                        </svg><span class="side-menu__label">{{__('messages.students')}}</span></a>
                                </li>
                    @endcan

            <div class="slide-right" id="slide-right"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191"
                    width="24" height="24" viewBox="0 0 24 24">
                    <path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z" />
                </svg></div>
        </div>
    </aside>
</div>
