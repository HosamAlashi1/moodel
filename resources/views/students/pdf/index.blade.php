<!DOCTYPE html>
<html>
<head>
    <title>Generate PDF File</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<h1>{{ $title }}</h1>
<p>{{ $date }}</p>
<div class="container">
    <table class="table table-bordered">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Age</th>
            <th>Courses</th>
        </tr>
        @foreach($students as $student)
            <tr>
                <th>{{ $student->id }}</th>
                <td>{{ $student->name_en }}</td>
                <td>{{ $student->age_en }}</td>
                <td class="pt-3">
                    @foreach($student->course_students as $course_student)
                            {{$course_student->courses->name_en}} :
                        @foreach($course_student->courses->course_teachers as $teacher)
                            @if($teacher->teachers)
                                    {{$teacher->teachers->name_en}},
                            @endif
                        @endforeach
                        <br>
                    @endforeach
                </td>
            </tr>
        @endforeach
    </table>
</div>
</body>
</html>
