@can('edit student')
    <a href="{{route('student.edit',$id)}}" type="button" class="btn btn-primary">{{__('messages.Edit')}}</a>
@endcan
@can('delete student')
    <a href="{{route('student.delete',$id)}}" type="button" class="btn btn-danger">{{__('messages.Delete')}}</a>
@endcan
@can('display student courses')
    <a href="{{route('student.course.index',$id)}}" type="button" class="btn btn-success">{{__('messages.Courses')}}</a>
@endcan
